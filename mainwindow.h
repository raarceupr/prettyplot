#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setupgraph();
    ~MainWindow();
    void AddPointToGraph(double x, double y);
    void Plot();


private:
    Ui::MainWindow *ui;
    QVector<double> XX, YY;
    QVector<double> XXN, YYN;    
};

#endif // MAINWINDOW_H
